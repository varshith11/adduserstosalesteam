public class AddUserToSalesTeam {
	public static void addUser(Id Opp,List<User> users)
    {
        List<OpportunityTeamMember> oppTeamMember =new List<OpportunityTeamMember>();
        for(User u:users)
        {
            OpportunityTeamMember otm = new OpportunityTeamMember();
            otm.OpportunityId=Opp;
            otm.UserId=u.Id;
            otm.TeamMemberRole='Sales Manager';
            otm.OpportunityAccessLevel='Read';
            oppTeamMember.add(otm);
        }
      insert oppTeamMember;
    }
    public static void addUser(List<Opportunity> oppList,User user)
    {
        List<OpportunityTeamMember> oppTeamMember =new List<OpportunityTeamMember>();
        for(Opportunity opp:oppList)
        {
            OpportunityTeamMember otm = new OpportunityTeamMember();
            otm.OpportunityId=opp.Id;
            otm.UserId=user.Id;
            otm.TeamMemberRole='Sales Manager';
            otm.OpportunityAccessLevel='Read';
            oppTeamMember.add(otm);
        }
      insert oppTeamMember;
    }
}